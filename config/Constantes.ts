export const Constantes = {
  baseUrl: process.env.NEXT_PUBLIC_BASE_URL,
  siteName: process.env.NEXT_PUBLIC_SITE_NAME,
  sitePath:
    "" === process.env.NEXT_PUBLIC_PATH
      ? ""
      : "/" + process.env.NEXT_PUBLIC_PATH,
  appEnv: process.env.NEXT_PUBLIC_APP_ENV,
};

export const RolesUsuario = {
  ADMINISTRADOR: "ADMINISTRADOR",
  EXPOSITOR: "EXPOSITOR",
  PARTICIPANTE: "PARTICIPANTE",
  CONTROL: "CONTROL",
  CASUAL: "CASUAL",
};

export const EstadosEvento = {
  ACTIVO: "ACTIVO",
  TERMINADO: "TERMINADO",
  CANCELADO: "CANCELADO",
};

export const EstadosReserva = {
  PENDIENTE: "PENDIENTE",
  CERTIFICADO: "CERTIFICADO",
  ACEPTADO: "ACEPTADO",
  ASISTIO: "ASISTIO",
  FALTO: "FALTO",
};
