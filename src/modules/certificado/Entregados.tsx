import React, { ReactNode, useEffect, useState } from "react";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { ReservaType } from "@/components/types/usuariosCRUDTypes";
import { Button, Grid, Typography } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { InterpreteMensajes, leerCookie } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import { Constantes, EstadosEvento, EstadosReserva } from "../../../config";

import dayjs from "dayjs";

export default function Entregados() {
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<ReservaType[]>([]);
  //const [ambienteData, setAmbienteData] = useState<AmbienteType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombres y apellidos" },
    { campo: "persona", nombre: "Nomber del Evento" },
    { campo: "persona", nombre: "Fecha del evento" },
    { campo: "correo", nombre: "Fecha de inscripción" },
    { campo: "rol", nombre: "Estado de Inscripción" },

    { campo: "acciones", nombre: "Acciones" },
  ];
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    ReservaType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (eventoData, indexEvento) => [
      <Typography
        key={`${eventoData.id}-${indexEvento}-nombres`}
        variant={"body2"}
      >
        {`${eventoData.participante?.nombre} ${eventoData.participante?.apellido}`}
      </Typography>,
      <div key={`${eventoData.id}-${indexEvento}-apellidos`}>
        <Typography
          variant={"body2"}
        >{`${eventoData.evento?.nombreEvento}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.evento?.fecha} ${dayjs(
          eventoData.evento?.horaInicio
        ).format("HH:mm")}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-edad`}>
        <Typography variant={"body2"}>{`${eventoData.fecha}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.estado}`}</Typography>
      </div>,
      <Grid key={`${eventoData.id}-${indexEvento}-acciones`}>
        <Button
          disabled={eventoData.estado === EstadosEvento.CANCELADO}
          key={`modificarEvento`}
          variant={"contained"}
          sx={{ ml: 1, mr: 1, textTransform: "none" }}
          color="secondary"
          size={"small"}
          onClick={() => {
            aceptarInscripcion(eventoData);
          }}
        >
          Descarga el Certificado
        </Button>
      </Grid>,
    ]
  );

  const aceptarInscripcion = async (reserva: ReservaType) => {
    try {
      //setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/reserva/${reserva.id}`,
        body: {
          estado: EstadosReserva.ACEPTADO,
        },
        tipo: "patch",
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      //accionCorrecta();
    } catch (e) {
      imprimir(`Error al reservar el evento: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      obtenerEventos();
    }
  };
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerEventos();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
  ];
  useEffect(() => {
    obtenerEventos();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite]);
  const obtenerEventos = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/entregados`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };

  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  return (
    <>
      <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
        disableBackdropClick
      >
        {/* <VistaModalEvento
          ambientes={ambienteData}
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        /> */}
      </CustomDialog>
      <CustomDataTable
        titulo={"Inscritos a varios eventos activos"}
        error={!!errorData}
        cargando={loading}
        acciones={acciones}
        columnas={columnas}
        contenidoTabla={contenidoTabla}
        paginacion={
          <Paginacion
            pagina={pagina}
            limite={limite}
            total={total}
            cambioPagina={setPagina}
            cambioLimite={setLimite}
          />
        }
      />
    </>
  );
}
