/// Vista modal de bono
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Box, Button, DialogActions, DialogContent, Grid } from "@mui/material";
import { useAlerts, useSession } from "@/components/hooks";
import { Constantes } from "../../../config";
import { InterpreteMensajes } from "@/components/utils";
import { imprimir } from "@/components/utils/imprimir";
import { FormInputText } from "@/components/form";
import ProgresoLineal from "@/components/ui/ProgresoLineal";
import { datosPagina, useAuth } from "@/components/context/auth";

export interface ModalEntidadType {
  accionCorrecta: () => void;
  accionCancelar: () => void;
}

export const ConfiguracionPagina = ({
  accionCorrecta,
  accionCancelar,
}: ModalEntidadType) => {
  const { datosPagina } = useAuth();
  const [loadingModal, setLoadingModal] = useState<boolean>(false);
  const { Alerta } = useAlerts();
  const { sesionPeticion } = useSession();
  // const [entidadesConsultaData, setEntidadesConsultaData] = useState<
  //   EntidadConsultaType[]
  // >([])
  // const [loading, setLoading] = useState<boolean>(false)

  const { handleSubmit, control } = useForm<datosPagina>({
    defaultValues: {
      mision: datosPagina.mision,
      nombreInstitucion: datosPagina.nombreInstitucion,
      objetivo: datosPagina.objetivo,
      quienes: datosPagina.quienes,
      vision: datosPagina.vision,
    },
  });
  // const onSelectEntidad = (dato: EntidadConsultaType) => {
  //   console.log(dato)
  //   if (dato) {
  //     setValue('nombre', dato.descEntidad)
  //     setValue('sigla', dato.siglaEntidad)
  //     setValue('idEntidadSigep', String(dato.idEntidad))
  //     setValue('codEntidad', String(dato.entidad))
  //   } else {
  //     onClear()
  //   }
  // }

  const guardarActualizarUsuarioPeticion = async (datosPage: datosPagina) => {
    try {
      setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/configuracion-web`,
        tipo: "patch",
        body: {
          ...datosPage,
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      accionCorrecta();
    } catch (e) {
      imprimir(`Error al crear o actualizar la entidad: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoadingModal(false);
    }
  };

  return (
    <>
      <DialogContent dividers>
        <Grid container direction={"column"} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}></Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"nombreInstitucion"}
                control={control}
                name="nombreInstitucion"
                label="Nombre de la Institución"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"mision"}
                control={control}
                name="mision"
                label="Misión"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                multiline={true}
                rows={2}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"vision"}
                control={control}
                name="vision"
                label="Visión"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                multiline={true}
                rows={2}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"quienes"}
                control={control}
                name="quienes"
                label="¿Quíenes somos?"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                multiline={true}
                rows={2}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"objetivo"}
                control={control}
                name="objetivo"
                label="Objetivos"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                multiline={true}
                rows={2}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid>
          <Box height={"20px"} />
          <ProgresoLineal mostrar={loadingModal} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: "flex-end",
            md: "flex-end",
            xs: "center",
            sm: "center",
          },
        }}
      >
        <Button
          variant={"outlined"}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant={"contained"}
          disabled={loadingModal}
          onClick={handleSubmit(guardarActualizarUsuarioPeticion)}
        >
          Guardar
        </Button>
      </DialogActions>
    </>
  );
};
