/// Vista modal de bono
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Box, Button, DialogActions, DialogContent, Grid } from "@mui/material";
import { useAlerts, useSession } from "@/components/hooks";
import { UsuarioRolCRUDType } from "@/components/types/usuariosCRUDTypes";
import { Constantes, RolesUsuario } from "../../../config";
import { InterpreteMensajes } from "@/components/utils";
import { imprimir } from "@/components/utils/imprimir";
import { FormInputDropdown, FormInputText } from "@/components/form";
import ProgresoLineal from "@/components/ui/ProgresoLineal";

export interface ModalEntidadType {
  usuario?: UsuarioRolCRUDType | undefined | null;
  accionCorrecta: () => void;
  accionCancelar: () => void;
}

export const VistaModalUsuario = ({
  usuario,
  accionCorrecta,
  accionCancelar,
}: ModalEntidadType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false);
  const { Alerta } = useAlerts();
  const { sesionPeticion } = useSession();
  // const [entidadesConsultaData, setEntidadesConsultaData] = useState<
  //   EntidadConsultaType[]
  // >([])
  // const [loading, setLoading] = useState<boolean>(false)

  const { handleSubmit, control } = useForm<UsuarioRolCRUDType>({
    defaultValues: {
      nombre: usuario?.nombre,
      correo: usuario?.correo,
      sexo: usuario?.sexo,
      apellido: usuario?.apellido,
      contrasena: usuario?.contrasena,
      edad: usuario?.edad,
      telefono: usuario?.telefono,
      direccion: usuario?.direccion,
      id: usuario?.id,
      idRol: usuario?.idRol,
    },
  });
  // const onSelectEntidad = (dato: EntidadConsultaType) => {
  //   console.log(dato)
  //   if (dato) {
  //     setValue('nombre', dato.descEntidad)
  //     setValue('sigla', dato.siglaEntidad)
  //     setValue('idEntidadSigep', String(dato.idEntidad))
  //     setValue('codEntidad', String(dato.entidad))
  //   } else {
  //     onClear()
  //   }
  // }

  const guardarActualizarUsuarioPeticion = async (
    usuario: UsuarioRolCRUDType
  ) => {
    try {
      setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios${
          usuario.id ? `/${usuario.id}` : ""
        }`,
        tipo: !!usuario.id ? "patch" : "post",
        body: {
          ...usuario,
          edad: Number(usuario.edad),
          telefono: Number(usuario.telefono),
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      accionCorrecta();
    } catch (e) {
      imprimir(`Error al crear o actualizar la entidad: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoadingModal(false);
    }
  };

  return (
    <>
      <DialogContent dividers>
        <Grid container direction={"column"} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"correo"}
                control={control}
                name="correo"
                label="Correo Electrónico"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"email"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"contrasena"}
                control={control}
                name="contrasena"
                label="Contraseña"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"password"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"nombre"}
                control={control}
                name="nombre"
                label="Nombres"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"apellido"}
                control={control}
                name="apellido"
                label="Apellidos"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"edad"}
                control={control}
                name="edad"
                label="Edad"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"number"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"direccion"}
                control={control}
                name="direccion"
                label="Dirección"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"telefono"}
                control={control}
                name="telefono"
                label="Telefóno"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"number"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"sexo"}
                control={control}
                name="sexo"
                label="Sexo"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={[
                  {
                    key: "M",
                    value: "M",
                    label: "Masculino",
                  },
                  {
                    key: "F",
                    value: "F",
                    label: "Femenino",
                  },
                ]}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"idRol"}
                control={control}
                name="idRol"
                label="Rol"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={[
                  {
                    label: RolesUsuario.ADMINISTRADOR,
                    key: "1",
                    value: "1",
                  },
                  {
                    label: RolesUsuario.EXPOSITOR,
                    key: "2",
                    value: "2",
                  },
                  {
                    label: RolesUsuario.PARTICIPANTE,
                    key: "3",
                    value: "3",
                  },
                  {
                    label: RolesUsuario.CONTROL,
                    key: "4",
                    value: "4",
                  },
                  {
                    label: RolesUsuario.CASUAL,
                    key: "5",
                    value: "5",
                  },
                ]}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid>
          <Box height={"20px"} />
          <ProgresoLineal mostrar={loadingModal} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: "flex-end",
            md: "flex-end",
            xs: "center",
            sm: "center",
          },
        }}
      >
        <Button
          variant={"outlined"}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant={"contained"}
          disabled={loadingModal}
          onClick={handleSubmit(guardarActualizarUsuarioPeticion)}
        >
          Guardar
        </Button>
      </DialogActions>
    </>
  );
};
