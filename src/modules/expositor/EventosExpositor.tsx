import UploadFileIcon from "@mui/icons-material/UploadFile";
import React, { ReactNode, useEffect, useState } from "react";
import { useAuth } from "@/components/context/auth";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { EventoType } from "@/components/types/usuariosCRUDTypes";
import { Stack, Fab, Grid, Typography, Tooltip } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { Constantes, EstadosEvento } from "../../../config";

import dayjs from "dayjs";

export default function EventosExpositor() {
  const { usuario } = useAuth();
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<EventoType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombre del Evento" },
    { campo: "persona", nombre: "Hora de inicio" },
    { campo: "persona", nombre: "Hora de fin" },
    { campo: "correo", nombre: "Fecha" },
    { campo: "rol", nombre: "Tipo" },
    { campo: "edad", nombre: "Modalidad" },
    { campo: "acciones", nombre: "Estado" },
    { campo: "acciones", nombre: "Acciones" },
  ];
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (eventoData, indexEvento) => [
      <Typography
        key={`${eventoData.id}-${indexEvento}-nombres`}
        variant={"body2"}
      >
        {`${eventoData.nombreEvento}`}
      </Typography>,
      <div key={`${eventoData.id}-${indexEvento}-apellidos`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horaInicio).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horafin).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-edad`}>
        <Typography variant={"body2"}>{`${eventoData.fecha}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.tipo}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.modalidad}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.estado}`}</Typography>
      </div>,
      /*       <Grid key={`${eventoData.id}-${indexEvento}-rol`}>
        <Chip
          key={`usuario-rol-${eventoData.estado}`}
          label={eventoData.estado}
        />
      </Grid>, */

      <Grid key={`${eventoData.id}-${indexEvento}-acciones`}>
        {/* <IconButton
          disabled={eventoData.estado === EstadosEvento.CANCELADO}
          aria-label="fingerprint"
          color="primary"
        > */}
        <Stack direction={"row"} spacing={1}>
          <Tooltip title="Subir material" arrow>
            <Fab
              disabled={eventoData.estado === EstadosEvento.CANCELADO}
              color="primary"
              aria-label="add"
              size="small"
              onClick={() => {
                //editarExpositorModal(eventoData);
              }}
            >
              <UploadFileIcon fontSize="small" />
            </Fab>
          </Tooltip>
        </Stack>
        {/* </IconButton> */}
      </Grid>,
    ]
  );
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerEventos();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
  ];
  useEffect(() => {
    obtenerEventos();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite]);
  const obtenerEventos = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/expositor/${usuario?.id}`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      {/* <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={usuarioEdicion ? "Editar Evento" : "Nuevo Evento"}
        disableBackdropClick
      >
        <VistaModalEvento
          ambientes={ambienteData}
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        />
      </CustomDialog> */}
      {/* <CustomDialog
        isOpen={modalExpositor}
        handleClose={cerrarModalExpositor}
        title={"Agregar Expositores"}
        disableBackdropClick
      >
        <VistaModalExpositor
          evento={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalExpositor().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalExpositor}
        />
      </CustomDialog> */}
      <CustomDataTable
        titulo={"Eventos donde Expone"}
        error={!!errorData}
        cargando={loading}
        acciones={acciones}
        columnas={columnas}
        contenidoTabla={contenidoTabla}
        paginacion={
          <Paginacion
            pagina={pagina}
            limite={limite}
            total={total}
            cambioPagina={setPagina}
            cambioLimite={setLimite}
          />
        }
      />
    </>
  );
}
