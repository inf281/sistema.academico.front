// Form Login

export interface LoginType {
  correo: string;
  contrasena: string;
}

export interface idRolType {
  idRol: string;
}

/// Usuario que iniciar sesión

export interface PropiedadesType {
  icono?: string;
  descripcion?: string;
  orden: number;
}

export interface SubModuloType {
  id: string;
  label: string;
  url: string;
  nombre: string;
  propiedades: PropiedadesType;
  estado: string;
}

export interface ModuloType {
  id: string;
  label: string;
  url: string;
  nombre: string;
  propiedades: PropiedadesType;
  estado: string;
  subModulo: SubModuloType[];
}

export interface PersonaType {
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  tipoDocumento: string;
  nroDocumento: string;
  fechaNacimiento: string;
}

export interface UsuarioType {
  id: string;
  correo: string;
  nombre: string;
  sexo: string;
  apellido: string;
  contrasena: string;
  edad: number;
  telefono: number;
  direccion: string;
  rol: string;
  idRol: string;
  toke: string;
}

export interface UsuarioRegisterType {
  correo: string;
  nombre: string;
  sexo: string;
  apellido: string;
  contrasena: string;
  edad: number | null;
  telefono: number | null;
  direccion: string;
  repiteContrasena: string;
}
export interface PoliticaType {
  sujeto: string;
  objeto: string;
  accion: string;
}
