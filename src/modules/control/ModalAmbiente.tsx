/// Vista modal de bono
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Box, Button, DialogActions, DialogContent, Grid } from "@mui/material";
import { useAlerts, useSession } from "@/components/hooks";
import { AmbienteType } from "@/components/types/usuariosCRUDTypes";
import { Constantes } from "../../../config";
import { InterpreteMensajes } from "@/components/utils";
import { imprimir } from "@/components/utils/imprimir";
import { FormInputText } from "@/components/form";
import ProgresoLineal from "@/components/ui/ProgresoLineal";

export interface ModalEntidadType {
  usuario?: AmbienteType | undefined | null;
  accionCorrecta: () => void;
  accionCancelar: () => void;
}

export const VistaModalAmbiente = ({
  usuario,
  accionCorrecta,
  accionCancelar,
}: ModalEntidadType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false);
  const { Alerta } = useAlerts();
  const { sesionPeticion } = useSession();
  // const [entidadesConsultaData, setEntidadesConsultaData] = useState<
  //   EntidadConsultaType[]
  // >([])
  // const [loading, setLoading] = useState<boolean>(false)

  const { handleSubmit, control } = useForm<AmbienteType>({
    defaultValues: {
      id: usuario?.id,
      nombreAmbiente: usuario?.nombreAmbiente,
      capacidad: usuario?.capacidad,
      asientos: usuario?.asientos,
      descripcion: usuario?.descripcion,
      estado: usuario?.estado,
      direccion: usuario?.direccion,
    },
  });
  // const onSelectEntidad = (dato: EntidadConsultaType) => {
  //   console.log(dato)
  //   if (dato) {
  //     setValue('nombre', dato.descEntidad)
  //     setValue('sigla', dato.siglaEntidad)
  //     setValue('idEntidadSigep', String(dato.idEntidad))
  //     setValue('codEntidad', String(dato.entidad))
  //   } else {
  //     onClear()
  //   }
  // }

  const guardarActualizarUsuarioPeticion = async (usuario: AmbienteType) => {
    try {
      setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/ambiente${
          usuario.id ? `/${usuario.id}` : ""
        }`,
        tipo: !!usuario.id ? "patch" : "post",
        body: {
          ...usuario,
          capacidad: Number(usuario.capacidad),
          asientos: Number(usuario.asientos),
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      accionCorrecta();
    } catch (e) {
      imprimir(`Error al crear o actualizar el ambiente: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoadingModal(false);
    }
  };

  return (
    <>
      <DialogContent dividers>
        <Grid container direction={"column"} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"nombreAmbiente"}
                control={control}
                name="nombreAmbiente"
                label="Nombre del Ambiente"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"email"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"descripcion"}
                control={control}
                name="descripcion"
                multiline
                label="Descripción"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"capacidad"}
                control={control}
                name="capacidad"
                label="Capacidad"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"number"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"asientos"}
                control={control}
                name="asientos"
                label="Asientos"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"number"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"direccion"}
                control={control}
                name="direccion"
                label="Direccion"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                multiline
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid>
          <Box height={"20px"} />
          <ProgresoLineal mostrar={loadingModal} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: "flex-end",
            md: "flex-end",
            xs: "center",
            sm: "center",
          },
        }}
      >
        <Button
          variant={"outlined"}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant={"contained"}
          disabled={loadingModal}
          onClick={handleSubmit(guardarActualizarUsuarioPeticion)}
        >
          Guardar
        </Button>
      </DialogActions>
    </>
  );
};
