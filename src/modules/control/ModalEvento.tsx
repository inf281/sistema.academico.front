/// Vista modal de bono

import { useState } from "react";
import { useForm } from "react-hook-form";
import {
  Stack,
  Box,
  Button,
  DialogActions,
  DialogContent,
  Grid,
} from "@mui/material";
import { useAlerts, useSession } from "@/components/hooks";
import { AmbienteType, EventoType } from "@/components/types/usuariosCRUDTypes";
import { Constantes } from "../../../config";
import { InterpreteMensajes } from "@/components/utils";
import { imprimir } from "@/components/utils/imprimir";
import {
  FormInputDate,
  FormInputDropdown,
  FormInputText,
} from "@/components/form";
import ProgresoLineal from "@/components/ui/ProgresoLineal";
import { FormInputTime } from "@/components/form/FormInputTime";

export interface ModalEntidadType {
  usuario?: EventoType | undefined | null;
  ambientes: Array<AmbienteType>;
  accionCorrecta: () => void;
  accionCancelar: () => void;
}

export const VistaModalEvento = ({
  usuario,
  ambientes = [],
  accionCorrecta,
  accionCancelar,
}: ModalEntidadType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false);
  const { Alerta } = useAlerts();
  const { sesionPeticion } = useSession();
  // const [entidadesConsultaData, setEntidadesConsultaData] = useState<
  //   EntidadConsultaType[]
  // >([])
  // const [loading, setLoading] = useState<boolean>(false)

  const { handleSubmit, control } = useForm<EventoType>({
    defaultValues: {
      id: usuario?.id,
      ambiente: usuario?.ambiente,
      estado: usuario?.estado,
      fecha: usuario?.fecha,
      horafin: usuario?.horafin,
      horaInicio: usuario?.horaInicio,
      modalidad: usuario?.modalidad,
      nombreEvento: usuario?.nombreEvento,
      tipo: usuario?.tipo,
      turno: usuario?.turno,
      idAmbiente: usuario?.idAmbiente,
    },
  });
  // const onSelectEntidad = (dato: EntidadConsultaType) => {
  //   console.log(dato)
  //   if (dato) {
  //     setValue('nombre', dato.descEntidad)
  //     setValue('sigla', dato.siglaEntidad)
  //     setValue('idEntidadSigep', String(dato.idEntidad))
  //     setValue('codEntidad', String(dato.entidad))
  //   } else {
  //     onClear()
  //   }
  // }

  const guardarActualizarUsuarioPeticion = async (usuario: EventoType) => {
    try {
      setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos${
          usuario.id ? `/${usuario.id}` : ""
        }`,
        tipo: !!usuario.id ? "patch" : "post",
        body: {
          ...usuario,
          fecha: usuario.fecha,
          horaInicio: usuario.horaInicio,
          horafin: usuario.horafin,
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      accionCorrecta();
    } catch (e) {
      imprimir(`Error al crear o actualizar el ambiente: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoadingModal(false);
    }
  };

  return (
    <>
      <DialogContent dividers>
        <Grid container direction={"column"} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputText
                id={"nombreEvento"}
                control={control}
                name="nombreEvento"
                label="Nombre del Evento"
                size={"medium"}
                labelVariant={"subtitle1"}
                type={"text"}
                disabled={loadingModal}
                rules={{
                  required: "Este campo es requerido",
                  minLength: {
                    value: 3,
                    message: "Mínimo 3 caracteres",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <Stack direction="row" spacing={2}>
                <FormInputDate
                  id={"fecha"}
                  size={"medium"}
                  control={control}
                  name="fecha"
                  label="Fecha"
                  disabled={loadingModal}
                  rules={{ required: "Este campo es requerido" }}
                />
                <FormInputTime
                  id={"horaInicio"}
                  size={"medium"}
                  control={control}
                  name="horaInicio"
                  label="Hora inicio"
                  disabled={loadingModal}
                  rules={{ required: "Este campo es requerido" }}
                />
                <FormInputTime
                  id={"horafin"}
                  size={"medium"}
                  control={control}
                  name="horafin"
                  label="Hora Fin"
                  disabled={loadingModal}
                  rules={{ required: "Este campo es requerido" }}
                />
              </Stack>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"Turno"}
                control={control}
                name="turno"
                label="Turno"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={[
                  {
                    key: "DIURNO",
                    value: "DIURNO",
                    label: "DIURNO",
                  },
                  {
                    key: "NOCTURNO",
                    value: "NOCTURNO",
                    label: "NOCTURNO",
                  },
                  {
                    key: "MATUTINO",
                    value: "MATUTINO",
                    label: "MATUTINO",
                  },
                  {
                    key: "VESPERTINO",
                    value: "VESPERTINO",
                    label: "VESPERTINO",
                  },
                  {
                    key: "INTERMEDIO",
                    value: "INTERMEDIO",
                    label: "INTERMEDIO",
                  },
                ]}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"tipo"}
                control={control}
                name="tipo"
                label="Tipo de evento"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={[
                  {
                    key: "CONFERENCIAS",
                    value: "CONFERENCIAS",
                    label: "CONFERENCIAS",
                  },
                  {
                    key: "SEMINARIOS",
                    value: "SEMINARIOS",
                    label: "SEMINARIOS",
                  },
                  {
                    key: "TALLERES",
                    value: "TALLERES",
                    label: "TALLERES",
                  },
                  {
                    key: "SIMPOSIOS",
                    value: "SIMPOSIOS",
                    label: "SIMPOSIOS",
                  },
                  {
                    key: "CURSOS Y DIPLOMADOS",
                    value: "CURSOS Y DIPLOMADOS",
                    label: "CURSOS Y DIPLOMADOS",
                  },
                  {
                    key: "FERIAS EDUCATIVAS",
                    value: "FERIAS EDUCATIVAS",
                    label: "FERIAS EDUCATIVAS",
                  },
                  {
                    key: "EXPOSICIONES",
                    value: "EXPOSICIONES",
                    label: "EXPOSICIONES",
                  },
                  {
                    key: "CHARLAS Y COLOQUIOS",
                    value: "CHARLAS Y COLOQUIOS",
                    label: "CHARLAS Y COLOQUIOS",
                  },
                  {
                    key: "CONCIERTOS Y FESTIVALES CULTURALES",
                    value: "CONCIERTOS Y FESTIVALES CULTURALES",
                    label: "CONCIERTOS Y FESTIVALES CULTURALES",
                  },
                  {
                    key: "ACTIVIDADES DEPORTIVAS Y RECREATIVAS",
                    value: "ACTIVIDADES DEPORTIVAS Y RECREATIVAS",
                    label: "ACTIVIDADES DEPORTIVAS Y RECREATIVAS",
                  },
                  {
                    key: "MESAS REDONDAS",
                    value: "MESAS REDONDAS",
                    label: "MESAS REDONDAS",
                  },
                ]}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"modalidad"}
                control={control}
                name="modalidad"
                label="Modalidad"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={[
                  {
                    key: "PRESENCIAL",
                    value: "PRESENCIAL",
                    label: "PRESENCIAL",
                  },
                  {
                    key: "VIRTUAL",
                    value: "VIRTUAL",
                    label: "VIRTUAL",
                  },
                  {
                    key: "SEMIPRESENCIAL",
                    value: "SEMIPRESENCIAL",
                    label: "SEMIPRESENCIAL",
                  },
                ]}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormInputDropdown
                id={"ambiente"}
                control={control}
                name="idAmbiente"
                label="Ambiente"
                size={"medium"}
                labelVariant={"subtitle1"}
                disabled={loadingModal}
                options={ambientes.map((item: AmbienteType) => {
                  return {
                    key: item.id,
                    value: item.id,
                    label: item.nombreAmbiente,
                  };
                })}
                rules={{
                  required: "Este campo es requerido",
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid>
          <Box height={"20px"} />
          <ProgresoLineal mostrar={loadingModal} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: "flex-end",
            md: "flex-end",
            xs: "center",
            sm: "center",
          },
        }}
      >
        <Button
          variant={"outlined"}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button
          variant={"contained"}
          disabled={loadingModal}
          onClick={handleSubmit(guardarActualizarUsuarioPeticion)}
        >
          Guardar
        </Button>
      </DialogActions>
    </>
  );
};
