import React, { ReactNode, useEffect, useState } from "react";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { AmbienteType } from "@/components/types/usuariosCRUDTypes";
import { Button, Grid, Typography } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import { Constantes } from "../../../config";
import { VistaModalAmbiente } from "./ModalAmbiente";

const columnas: Array<ColumnaType> = [
  { campo: "nombre", nombre: "Nombre del Ambiente" },
  { campo: "correo", nombre: "Descripción" },
  { campo: "rol", nombre: "Capacidad" },
  { campo: "edad", nombre: "Asientos" },
  { campo: "acciones", nombre: "Dirección" },
  { campo: "acciones", nombre: "Acciones" },
];
export default function Ambiente() {
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<AmbienteType[]>([]);
  const [errorData, setErrorData] = useState<any>();

  const [ambienteEdicion, setAmbienteEdicion] = useState<
    AmbienteType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const editarUsuarioModal = (usuario: AmbienteType) => {
    setAmbienteEdicion(usuario);
    setModalUsuario(true);
  };
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (ambienteData, indexAmbiente) => [
      <Typography
        key={`${ambienteData.id}-${indexAmbiente}-nombres`}
        variant={"body2"}
      >
        {`${ambienteData.nombreAmbiente}`}
      </Typography>,

      <div key={`${ambienteData.id}-${indexAmbiente}-edad`}>
        <Typography
          variant={"body2"}
        >{`${ambienteData.descripcion}`}</Typography>
      </div>,
      <div key={`${ambienteData.id}-${indexAmbiente}-correo`}>
        <Typography variant={"body2"}>{`${ambienteData.capacidad}`}</Typography>
      </div>,
      <div key={`${ambienteData.id}-${indexAmbiente}-correo`}>
        <Typography variant={"body2"}>{`${ambienteData.asientos}`}</Typography>
      </div>,
      <div key={`${ambienteData.id}-${indexAmbiente}-correo`}>
        <Typography variant={"body2"}>{`${ambienteData.direccion}`}</Typography>
      </div>,
      /*       <Grid key={`${ambienteData.id}-${indexAmbiente}-rol`}>
        <Chip
          key={`usuario-rol-${ambienteData.estado}`}
          label={ambienteData.estado}
        />
      </Grid>, */

      <Grid key={`${ambienteData.id}-${indexAmbiente}-acciones`}>
        <Button
          key={`accionAgregarUsuarioBoton`}
          variant={"contained"}
          sx={{ ml: 1, mr: 1, textTransform: "none" }}
          color="primary"
          size={"small"}
          onClick={() => {
            imprimir(`reservaremos`, ambienteData);
            editarUsuarioModal(ambienteData);
          }}
        >
          Modificar
        </Button>
        {/* <Button
          key={`accionAgregarUsuarioBoton`}
          variant={"contained"}
          sx={{ ml: 1, mr: 1, textTransform: "none" }}
          color="error"
          size={"small"}
          onClick={() => {
            imprimir(`reservaremos`, ambienteData);
            reservar(ambienteData);
          }}
        >
          Desactivar
        </Button> */}
      </Grid>,
    ]
  );
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerAmbiente();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
    <Button
      key={`accionAgregarUsuarioBoton`}
      variant={"contained"}
      sx={{ ml: 1, mr: 1, textTransform: "none" }}
      color="primary"
      size={"small"}
      onClick={() => {
        agregarUsuarioModal();
      }}
    >
      Agregar ambiente
    </Button>,
  ];
  useEffect(() => {
    obtenerAmbiente();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite]);
  const obtenerAmbiente = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/ambiente`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  const agregarUsuarioModal = () => {
    setAmbienteEdicion(null);
    setModalUsuario(true);
  };
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setAmbienteEdicion(null);
  };

  return (
    <>
      <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={ambienteEdicion ? "Editar usuario" : "Nuevo usuario"}
        disableBackdropClick
      >
        <VistaModalAmbiente
          usuario={ambienteEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerAmbiente().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        />
      </CustomDialog>

      <CustomDataTable
        titulo={"Ambientes"}
        error={!!errorData}
        cargando={loading}
        acciones={acciones}
        columnas={columnas}
        contenidoTabla={contenidoTabla}
        paginacion={
          <Paginacion
            pagina={pagina}
            limite={limite}
            total={total}
            cambioPagina={setPagina}
            cambioLimite={setLimite}
          />
        }
      />
    </>
  );
}
