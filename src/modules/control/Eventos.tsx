import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import ToggleOffIcon from "@mui/icons-material/ToggleOff";
import ToggleOnIcon from "@mui/icons-material/ToggleOn";
import EditIcon from "@mui/icons-material/Edit";
import React, { ReactNode, useEffect, useState } from "react";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { AmbienteType, EventoType } from "@/components/types/usuariosCRUDTypes";
import { Stack, Button, Fab, Grid, Typography, Tooltip } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import { Constantes, EstadosEvento } from "../../../config";
import { VistaModalEvento } from "./ModalEvento";
import dayjs from "dayjs";
import VistaModalExpositor from "./ModalExpositores";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

/* function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
 */
export default function Eventos() {
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<EventoType[]>([]);
  const [ambienteData, setAmbienteData] = useState<AmbienteType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombre del Evento" },
    { campo: "persona", nombre: "Hora de inicio" },
    { campo: "persona", nombre: "Hora de fin" },
    { campo: "correo", nombre: "Fecha" },
    { campo: "rol", nombre: "Tipo" },
    { campo: "edad", nombre: "Modalidad" },
    { campo: "acciones", nombre: "Estado" },
    { campo: "acciones", nombre: "Acciones" },
  ];
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    EventoType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const [modalExpositor, setModalExpositor] = useState(false);
  const editarUsuarioModal = (usuario: EventoType) => {
    setUsuarioEdicion(usuario);
    setModalUsuario(true);
  };
  const editarExpositorModal = (usuario: EventoType) => {
    setUsuarioEdicion(usuario);
    setModalExpositor(true);
  };
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (eventoData, indexEvento) => [
      <Typography
        key={`${eventoData.id}-${indexEvento}-nombres`}
        variant={"body2"}
      >
        {`${eventoData.nombreEvento}`}
      </Typography>,
      <div key={`${eventoData.id}-${indexEvento}-apellidos`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horaInicio).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horafin).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-edad`}>
        <Typography variant={"body2"}>{`${eventoData.fecha}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.tipo}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.modalidad}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.estado}`}</Typography>
      </div>,
      /*       <Grid key={`${eventoData.id}-${indexEvento}-rol`}>
        <Chip
          key={`usuario-rol-${eventoData.estado}`}
          label={eventoData.estado}
        />
      </Grid>, */

      <Grid key={`${eventoData.id}-${indexEvento}-acciones`}>
        {/* <IconButton
          disabled={eventoData.estado === EstadosEvento.CANCELADO}
          aria-label="fingerprint"
          color="primary"
        > */}
        <Stack direction={"row"} spacing={1}>
          <Tooltip title="Agregar expositores" arrow>
            <span>
              <Fab
                disabled={eventoData.estado === EstadosEvento.CANCELADO}
                color="primary"
                aria-label="add"
                size="small"
                onClick={() => {
                  editarExpositorModal(eventoData);
                }}
              >
                <PersonAddAlt1Icon fontSize="small" />
              </Fab>
            </span>
          </Tooltip>
          <Tooltip title="Modificar" arrow>
            <span>
              <Fab
                disabled={eventoData.estado === EstadosEvento.CANCELADO}
                color="secondary"
                aria-label="add"
                size="small"
                onClick={() => {
                  editarUsuarioModal(eventoData);
                }}
              >
                <EditIcon fontSize="small" />
              </Fab>
            </span>
          </Tooltip>
          <Tooltip title="Desactivar" arrow>
            <span>
              <Fab
                color={
                  eventoData.estado === EstadosEvento.CANCELADO
                    ? "success"
                    : "error"
                }
                aria-label="add"
                size="small"
                onClick={async () => {
                  imprimir(`reservaremos`, eventoData);
                  await guardarActualizarUsuarioPeticion(eventoData);
                }}
              >
                {eventoData.estado === EstadosEvento.CANCELADO ? (
                  <ToggleOffIcon fontSize="small" />
                ) : (
                  <ToggleOnIcon fontSize="small" />
                )}
              </Fab>
            </span>
          </Tooltip>
        </Stack>
        {/* </IconButton> */}
      </Grid>,
    ]
  );
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerEventos();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
    <Button
      key={`accionAgregarUsuarioBoton`}
      variant={"contained"}
      sx={{ ml: 1, mr: 1, textTransform: "none" }}
      color="secondary"
      size={"small"}
      onClick={() => {
        imprimir(`reservaremos`);
        agregarUsuarioModal();
      }}
    >
      Agregar nuevo evento
    </Button>,
  ];
  const obtenerAmbiente = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/ambiente`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setAmbienteData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    obtenerAmbiente();
  }, []);
  useEffect(() => {
    obtenerEventos();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite]);
  const obtenerEventos = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  const guardarActualizarUsuarioPeticion = async (usuario: EventoType) => {
    try {
      setLoading(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos${
          usuario.id ? `/${usuario.id}` : ""
        }`,
        tipo: !!usuario.id ? "patch" : "post",
        body: {
          ...usuario,
          estado:
            usuario.estado === EstadosEvento.ACTIVO
              ? EstadosEvento.CANCELADO
              : EstadosEvento.ACTIVO,
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
    } catch (e) {
      imprimir(`Error al crear o actualizar el ambiente: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
      await obtenerEventos();
    }
  };
  const agregarUsuarioModal = () => {
    setUsuarioEdicion(null);
    setModalUsuario(true);
  };
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  const cerrarModalExpositor = async () => {
    setModalExpositor(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };
  return (
    <>
      <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={usuarioEdicion ? "Editar Evento" : "Nuevo Evento"}
        disableBackdropClick
      >
        <VistaModalEvento
          ambientes={ambienteData}
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        />
      </CustomDialog>
      <CustomDialog
        isOpen={modalExpositor}
        handleClose={cerrarModalExpositor}
        title={"Agregar Expositores"}
        disableBackdropClick
      >
        <VistaModalExpositor
          evento={usuarioEdicion}
          accionCancelar={cerrarModalExpositor}
        />
      </CustomDialog>
      <CustomDataTable
        titulo={"Eventos"}
        error={!!errorData}
        cargando={loading}
        acciones={acciones}
        columnas={columnas}
        contenidoTabla={contenidoTabla}
        paginacion={
          <Paginacion
            pagina={pagina}
            limite={limite}
            total={total}
            cambioPagina={setPagina}
            cambioLimite={setLimite}
          />
        }
      />
    </>
  );
}
