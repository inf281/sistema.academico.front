import React, {  useState } from "react";
import { EventoType } from "@/components/types/usuariosCRUDTypes";
import {
  DialogActions,
  DialogContent,
  Box,
  Button,
  Tab,
  Tabs,
} from "@mui/material";
import { CustomDialog } from "@/components/ui/CustomDialog";
import ExpositoresInscritos from "./ExpositoresInscritos";
import ExpositoresSinInscribir from "./ExpositoresSinInscribir";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
export interface ModalExpositoresType {
  evento: EventoType | undefined | null;
  accionCancelar: () => void;
}
export default function VistaModalExpositor({
  evento,
  accionCancelar,
}: ModalExpositoresType) {
  const [value, setValue] = React.useState(0);
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    EventoType | undefined | null
  >();
  const [modalUsuario, setModalUsuario] = useState(false);
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <DialogContent dividers>
        <CustomDialog
          isOpen={modalUsuario}
          handleClose={cerrarModalUsuario}
          title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
          disableBackdropClick
        >
          {/* <VistaModalUsuario
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        /> */}
        </CustomDialog>

        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="Expositores" {...a11yProps(0)} />
          <Tab label="Agregar expositor" {...a11yProps(1)} />
        </Tabs>

        <TabPanel value={value} index={0}>
          <ExpositoresInscritos evento={evento} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <ExpositoresSinInscribir evento={evento} />
        </TabPanel>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: "flex-end",
            md: "flex-end",
            xs: "center",
            sm: "center",
          },
        }}
      >
        <Button
          variant={"outlined"}
          onClick={accionCancelar}
        >
          Cerrar
        </Button>
      </DialogActions>
    </>
  );
}
