import React, { ReactNode, useEffect, useState } from "react";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { EventoType } from "@/components/types/usuariosCRUDTypes";
import { Button, Grid, Typography } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import { Constantes, EstadosEvento } from "../../../config";
import { UsuarioType } from "../login/types/loginTypes";

export interface ExpositoresInscritosType {
  evento: EventoType | undefined | null;
}
export default function ExpositoresSinInscribir({
  evento,
}: ExpositoresInscritosType) {
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<UsuarioType[]>([]);
  //const [ambienteData, setAmbienteData] = useState<AmbienteType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombres y apellidos" },
    { campo: "rol", nombre: "Correo Electrónico" },

    { campo: "acciones", nombre: "Acciones" },
  ];
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    UsuarioType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (eventoData, indexEvento) => [
      <Typography
        key={`${eventoData.id}-${indexEvento}-nombres`}
        variant={"body2"}
      >
        {`${eventoData?.nombre} ${eventoData?.apellido}`}
      </Typography>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.correo}`}</Typography>
      </div>,
      /*       <Grid key={`${eventoData.id}-${indexEvento}-rol`}>
        <Chip
          key={`usuario-rol-${eventoData.estado}`}
          label={eventoData.estado}
        />
      </Grid>, */

      <Grid key={`${eventoData.id}-${indexEvento}-acciones`}>
        <Button
          key={`cancelarEvento`}
          variant={"contained"}
          sx={{ ml: 1, mr: 1, textTransform: "none" }}
          color={"primary"}
          size={"small"}
          disabled={evento?.estado === EstadosEvento.CANCELADO}
          onClick={async () => {
            imprimir(`reservaremos`, eventoData);
            await aceptarInscripcion(eventoData);
          }}
        >
          Agregar
        </Button>
      </Grid>,
    ]
  );

  const aceptarInscripcion = async (usuario: UsuarioType) => {
    if (!evento) return;
    try {
      //setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/${evento.id}/expositores`,
        body: {
          expocitoresSelect: [usuario.id],
        },
        tipo: "patch",
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      //accionCorrecta();
    } catch (e) {
      imprimir(`Error al reservar el evento: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      obtenerEventos();
    }
  };
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerEventos();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
    /*     <Button
      key={`accionAgregarUsuarioBoton`}
      variant={"contained"}
      sx={{ ml: 1, mr: 1, textTransform: "none" }}
      color="secondary"
      size={"small"}
      onClick={() => {
        imprimir(`reservaremos`);
        agregarUsuarioModal();
      }}
    >
      Agregar nuevo evento
    </Button>, */
  ];
  useEffect(() => {
    obtenerEventos();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite, evento]);
  const obtenerEventos = async () => {
    if (!evento) return;
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/noExpositores/${evento.id}`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };

  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  return (
    <>
      <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
        disableBackdropClick
      >
        {/* <VistaModalEvento
          ambientes={ambienteData}
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        /> */}
      </CustomDialog>
      <CustomDataTable
        titulo={"Seleccione un expositor"}
        error={!!errorData}
        cargando={loading}
        acciones={acciones}
        columnas={columnas}
        contenidoTabla={contenidoTabla}
        paginacion={
          <Paginacion
            pagina={pagina}
            limite={limite}
            total={total}
            cambioPagina={setPagina}
            cambioLimite={setLimite}
          />
        }
      />
    </>
  );
}
