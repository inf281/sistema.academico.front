import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import {
  Control,
  Controller,
  FieldValues,
  Path,
  PathValue,
} from "react-hook-form";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import {
  FormHelperText,
  InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import { RegisterOptions } from "react-hook-form/dist/types/validator";
import esMX from "dayjs/locale/es-mx";
import { validarFechaFormato } from "../../components/utils/fechas";
import { Variant } from "@mui/material/styles/createTypography";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
type FormDatePickerProps<T extends FieldValues> = {
  id: string;
  name: Path<T>;
  control: Control<T, object>;
  label: string;
  size?: "small" | "medium";
  format?: string;
  disabled?: boolean;
  rules?: RegisterOptions;
  bgcolor?: string;
  labelVariant?: Variant;
};

export const FormInputTime = <T extends FieldValues>({
  id,
  name,
  control,
  label,
  size = "small",
  format = "DD/MM/YYYY",
  disabled,
  rules,
  bgcolor,
  labelVariant = "subtitle2",
}: FormDatePickerProps<T>) => {
  return (
    <div>
      <Controller
        name={name}
        control={control}
        render={({ field, fieldState: { error } }) => (
          <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={esMX}>
            <TimePicker
              onChange={field.onChange}
              value={field.value}
              ref={field.ref}
              //mask={"__/__/____"}
              //inputFormat={format}
              disabled={disabled}
              label={label}
              renderInput={(params: any) => (
                <>
                  <TextField
                    id={id}
                    name={name}
                    sx={{ width: "100%", bgcolor: bgcolor }}
                    size={size}
                    {...params}
                    error={!!error}
                  />
                  {!!error && (
                    <FormHelperText error>{error?.message}</FormHelperText>
                  )}
                </>
              )}
            />
          </LocalizationProvider>
        )}
        rules={{
          ...{
            validate: (val?: string) => {
              if (val && !validarFechaFormato(val, format)) {
                return "La fecha no es válida";
              }
            },
          },
          ...rules,
        }}
        defaultValue={"" as PathValue<T, Path<T>>}
      />
    </div>
  );
};
