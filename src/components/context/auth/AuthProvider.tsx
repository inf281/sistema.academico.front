import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { guardarCookie, InterpreteMensajes, leerCookie } from "../../utils";
import { Servicios } from "../../services";
import { Constantes, RolesUsuario } from "../../../../config";
import { useRouter } from "next/router";
import { useFullScreenLoading } from "../ui";
import { useAlerts, useSession } from "../../hooks";
import { imprimir } from "../../utils/imprimir";
import {
  idRolType,
  LoginType,
  UsuarioType,
} from "../../../modules/login/types/loginTypes";
import { log } from "console";
export interface datosPagina {
  nombreInstitucion: string;
  mision: string;
  vision: string;
  quienes: string;
  objetivo: string;
}
interface ContextProps {
  cargarUsuarioManual: () => Promise<void>;
  borrarSesionUsuario: () => Promise<void>;
  obtenerDatosPagina: () => Promise<void>;
  estaAutenticado: boolean;
  usuario: UsuarioType | null;
  rolUsuario: string | null | undefined;
  ingresar: ({ correo, contrasena }: LoginType) => Promise<void>;
  progresoLogin: boolean;
  datosPagina: datosPagina;
}

const AuthContext = createContext<ContextProps>({} as ContextProps);

interface AuthContextType {
  children: ReactNode;
}

export const AuthProvider = ({ children }: AuthContextType) => {
  const [user, setUser] = useState<UsuarioType | null>(null);
  const [rol, setRol] = useState<string | null>();
  const [loading, setLoading] = useState<boolean>(true);
  const [datosPagina, setDatosPagina] = useState<datosPagina>({
    mision: "",
    nombreInstitucion: "",
    objetivo: "",
    quienes: "",
    vision: "",
  });
  // Hook para mostrar alertas
  const { Alerta } = useAlerts();

  const { mostrarFullScreen, ocultarFullScreen } = useFullScreenLoading();

  const router = useRouter();

  const { cerrarSesion, sesionPeticion } = useSession();

  const inicializarUsuario = async () => {
    const token = leerCookie("token");

    if (!token) {
      await borrarSesionUsuario();
      await router.replace({
        pathname: "/",
      });
      setLoading(false);
      return;
    }

    try {
      mostrarFullScreen();

      const respuestaPeticion = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/session`,
        body: {
          token,
        },
        tipo: "POST",
      });
      if (respuestaPeticion.finalizado) {
        setUser(respuestaPeticion.datos);
        setRol(respuestaPeticion.datos.rol);
      } else {
        Alerta({ mensaje: `No se cargar la sesión`, variant: "error" });
        await borrarSesionUsuario();
      }
    } catch (error: Error | any) {
      imprimir(`Error durante inicializarUsuario 🚨`, typeof error, error);
      await borrarSesionUsuario();
      Alerta({ mensaje: `No se cargar la sesión`, variant: "error" });
    } finally {
      setLoading(false);
      ocultarFullScreen();
    }
  };

  const borrarSesionUsuario = async () => {
    setUser(null);
    setRol(null);
    await cerrarSesion();
  };
  const obtenerDatosPagina = async () => {
    try {
      setLoading(true);
      const respuesta = await Servicios.peticionHTTP({
        url: `${Constantes.baseUrl}/configuracion-web`,
        tipo: "get",
      });
      imprimir(respuesta.data);
      if (respuesta.data.finalizado) {
        const { mision, nombreInstitucion, objetivo, quienes, vision } =
          respuesta.data.datos;
        imprimir(respuesta.data);
        setDatosPagina({
          mision,
          nombreInstitucion,
          objetivo,
          quienes,
          vision,
        });
      } else {
        Alerta({ mensaje: `Error al obtener datos`, variant: "error" });
      }
    } catch (e) {
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  const cargarUsuarioManual = async () => {
    try {
      /* await obtenerUsuarioRol(); */
      //await obtenerPermisos()

      mostrarFullScreen();

      /* await router.replace({
        pathname: "/admin/home",
      }); */
    } catch (error: Error | any) {
      imprimir(`Error durante cargarUsuarioManual 🚨`, error);
      await borrarSesionUsuario();
      Alerta({ mensaje: `No se cargar la sesión`, variant: "error" });
      imprimir(`🚨 -> login`);
      /* await router.replace({
        pathname: "/login",
      }); */
      throw error;
    } finally {
      ocultarFullScreen();
    }
  };

  useEffect(() => {
    if (!router.isReady) return;
    inicializarUsuario()
      .catch(imprimir)
      .finally(() => {
        imprimir("Verificación de login finalizada 👨‍💻");
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.isReady]);

  useEffect(() => {
    obtenerDatosPagina()
      .catch(imprimir)
      .finally(() => {
        imprimir("Actualizacion de datos de la pagina 👨‍💻");
      });
  }, []);
  const login = async ({ correo, contrasena }: LoginType) => {
    try {
      setLoading(true);

      const respuesta = await Servicios.post({
        url: `${Constantes.baseUrl}/usuarios/login`,
        body: { correo, contrasena },
        headers: {},
      });

      if (respuesta.finalizado) {
        imprimir(respuesta);
        guardarCookie("token", respuesta.datos.token);
        imprimir(`Token ✅: ${respuesta.datos.token}`);
        setUser(respuesta.datos);
        setRol(respuesta.datos.rol);
        if (respuesta.datos.rol) await cambiaRutaRol(respuesta.datos.rol);
        else {
          borrarSesionUsuario();
        }
      } else {
        Alerta({ mensaje: `No se pudo iniciar sesión`, variant: "error" });
        await borrarSesionUsuario();
      }
      mostrarFullScreen();
    } catch (e) {
      imprimir(`Error al iniciar sesión: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
      await borrarSesionUsuario();
    } finally {
      setLoading(false);
      ocultarFullScreen();
    }
  };
  const cambiaRutaRol = async (rol: string) => {
    switch (rol) {
      case RolesUsuario.ADMINISTRADOR:
        await router.replace({
          pathname: "/administrador",
        });
        break;
      case RolesUsuario.PARTICIPANTE:
        await router.replace({
          pathname: "/participante",
        });
        break;
      case RolesUsuario.CONTROL:
        await router.replace({
          pathname: "/control",
        });
        break;

      case RolesUsuario.EXPOSITOR:
        await router.replace({
          pathname: "/expositor",
        });
        break;
      default:
        imprimir("entra aqui");
        await router.replace({
          pathname: "/ ",
        });

        break;
    }
  };
  return (
    <AuthContext.Provider
      value={{
        obtenerDatosPagina,
        cargarUsuarioManual,
        estaAutenticado: !!user && !loading,
        usuario: user,
        rolUsuario: rol,
        ingresar: login,
        progresoLogin: loading,
        borrarSesionUsuario,
        datosPagina: datosPagina,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
