import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import {
  ThemeProvider as MuiThemeProvider,
  useMediaQuery,
} from "@mui/material";

import { darkTheme, lightTheme } from "../../../themes";

import { useDebouncedCallback } from "use-debounce";
import { guardarCookie, leerCookie } from "../utils";
import { imprimir } from "../utils/imprimir";

const DARK_SCHEME_QUERY = "(prefers-color-scheme: dark)";

type ThemeMode = "light" | "dark";

interface ThemeContextType {

}

const ThemeContext = createContext<ThemeContextType>({} as ThemeContextType);
const useThemeContext = () => useContext(ThemeContext);

const ThemeProvider = ({ children }: { children: ReactNode }) => {

  return (
    <ThemeContext.Provider value={{ }}>
      <MuiThemeProvider theme={lightTheme}>{children}</MuiThemeProvider>
    </ThemeContext.Provider>
  );
};

export { useThemeContext, ThemeProvider };
