// CRUD de usuarios

import { UsuarioType } from "@/modules/login/types/loginTypes";

export interface EntidadCRUDType {
  id: string;
  estado: string;
  nombre: string;
  descripcion: string;
  sigla: string;
  idEntidadPago: string;
  codEntidad: string;
}

export interface RolCRUDType {
  id: string;
  rol: string;
}

export interface UsuarioRolCRUDType {
  id: string;
  correo: string;
  nombre: string;
  sexo: string;
  apellido: string;
  contrasena: string;
  edad: number;
  telefono: number;
  direccion: string;
  rol: RolType;
  token: string;
  idRol: string;
}

export interface ReservaType {
  id: string;
  fecha: Date;
  idParticipante: string;
  idEvento: string;
  estado: string;
  evento?: EventoType;
  participante?: UsuarioType;
}

export interface EventoType {
  id: string;
  tipo: string;
  nombreEvento: string;
  turno: string;
  modalidad: string;
  horaInicio: string;
  horafin: string;
  fecha: Date;
  estado: string;
  reserva: Array<ReservaType>;
  ambiente?: AmbienteType;
  idAmbiente?: string;
  expone?: ExponeType[];
  expocitoresSelect: string[]
}

export interface ExponeType {
  id: string;
  idExpositor: string
  idEvento?: string;
  fecha: string
  expositor: UsuarioType
  evento: EventoType
}
export interface AmbienteType {
  id: string;
  nombreAmbiente: string;
  capacidad: number;
  asientos: number;
  descripcion: string;
  estado: string;
  direccion: string;
}

export interface PersonaCRUDType {
  nombreCompleto: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  tipoDocumento: string;
  nroDocumento: string;
  fechaNacimiento: string;
  complementoDocumento: string;
  genero: string;
  contacto: {
    telefono: string;
  };
  domicilios: Array<DomicilioType>;
}

export interface DomicilioType {
  id: string;
  dpa: DpaType;
}

export interface DpaType {
  departamento: string;
  municipio: string;
  provincia: string;
}

// Crear usuario

export interface CrearPersonaType {
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  nroDocumento: string;
  fechaNacimiento: string;
  genero: string;
  contacto: {
    telefono: string;
  };
}

export interface CrearEditarUsuarioType {
  id?: string;
  usuario?: any;
  persona: CrearPersonaType;
  ciudadaniaDigital: boolean;
  roles: string[];
  idEntidad?: string;
  idBono: string;
  estado: string;
  correoElectronico: string;
}

/// Tipo rol transversal

export interface RolType {
  id: string;
  nombreRol: string;
}

export interface GenerosType {
  id: string;
  codigo: string;
  nombre: string;
}

export interface DpasType {
  id: string;
  estado: string;
  codMunicipio: string;
  municipio: string;
  codProvincia: string;
  provincia: string;
  codDepartamento: string;
  departamento: string;
}

export interface DepartamentosType {
  id: string;
  codigo: string;
  nombre: string;
}

export interface MunicipioType {
  id: string;
  codigo: string;
  nombre: string;
}

export interface CrearDpaUsuarioType {
  rol: string;
  bono: string;
  departamentos: string[];
  municipios: string[];
}

export interface RolDpaType {
  key: string;
  value: string;
  label: string;
  metadata: any;
}

export interface BonoDpaType {
  id: string;
  key: string;
  value: string;
  label: string;
  tipoAlcance: string;
}

export interface PermisosDpaType {
  idUsuario: string;
  idRol: string;
  metadata: any;
}