import * as React from "react";
import Settings from "@mui/icons-material/Settings";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Link from "next/link";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import IconButton from "@mui/material/IconButton";
import { Typography } from "@mui/material";
import Logout from "@mui/icons-material/Logout";
import { useAuth } from "../context/auth";
import { Stack } from "@mui/material";
import { CustomDialog } from "./CustomDialog";
import { UsuarioRolCRUDType } from "../types/usuariosCRUDTypes";
import { ConfiguracionPagina } from "@/modules/usuario/ConfiguracionPagina";
import { RolesUsuario } from "../../../config";
const drawerWidth = 240;
type Props = {
  title?: string;
  children: React.ReactNode;
  opciones?: React.ReactNode;
};
const NavBarDrawer: React.FC<Props> = ({ children, opciones }) => {
  const {
    estaAutenticado,
    usuario,
    borrarSesionUsuario,
    datosPagina,
    rolUsuario,
    obtenerDatosPagina,
  } = useAuth();
  const [usuarioEdicion, setUsuarioEdicion] = React.useState<
    UsuarioRolCRUDType | undefined | null
  >();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const [modalUsuario, setModalUsuario] = React.useState(false);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };
  const agregarUsuarioModal = () => {
    setUsuarioEdicion(null);
    setModalUsuario(true);
  };
  return (
    <>
      <CustomDialog
        isOpen={modalUsuario}
        handleClose={cerrarModalUsuario}
        title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
        disableBackdropClick
      >
        <ConfiguracionPagina
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerDatosPagina().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        />
      </CustomDialog>
      <Box sx={{ display: "flex" }}>
        <AppBar
          position="fixed"
          sx={{
            zIndex: (theme) => theme.zIndex.drawer + 1,
            backgroundColor: "black",
          }}
        >
          <Toolbar style={{ flexGrow: 1, justifyContent: "space-between" }}>
            <Link className="navbar-brand" href="/">
              <Stack direction={"row"} spacing={3} alignItems="center">
                <img src="../images/logoL.png" width="70px" height="50px" />
                <Typography>{datosPagina.nombreInstitucion}</Typography>
              </Stack>
            </Link>

            {estaAutenticado && (
              <>
                <Stack direction={"row"} spacing={3} alignItems="center">
                  <Typography>
                    {usuario?.nombre + " " + usuario?.apellido}
                  </Typography>
                  <IconButton
                    onClick={handleClick}
                    size="small"
                    sx={{ ml: 2 }}
                    aria-controls={open ? "account-menu" : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? "true" : undefined}
                  >
                    <Avatar sx={{ width: 50, height: 50 }}>
                      {String(usuario?.nombre ?? "N")
                        .charAt(0)
                        .toLocaleUpperCase()}
                    </Avatar>
                  </IconButton>
                  <Menu
                    anchorEl={anchorEl}
                    id="account-menu"
                    open={open}
                    onClose={handleClose}
                    onClick={handleClose}
                    PaperProps={{
                      elevation: 0,
                      sx: {
                        overflow: "visible",
                        filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                        mt: 1.5,
                        "& .MuiAvatar-root": {
                          width: 32,
                          height: 32,
                          ml: -0.5,
                          mr: 1,
                        },
                        "&:before": {
                          content: '""',
                          display: "block",
                          position: "absolute",
                          top: 0,
                          right: 14,
                          width: 10,
                          height: 10,
                          bgcolor: "background.paper",
                          transform: "translateY(-50%) rotate(45deg)",
                          zIndex: 0,
                        },
                      },
                    }}
                    transformOrigin={{ horizontal: "right", vertical: "top" }}
                    anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
                  >
                    {rolUsuario === RolesUsuario.ADMINISTRADOR && (
                      <MenuItem
                        onClick={() => {
                          agregarUsuarioModal();
                          handleClose();
                        }}
                      >
                        <ListItemIcon>
                          <Settings fontSize="small" />
                        </ListItemIcon>
                        Configuraciones
                      </MenuItem>
                    )}
                    <MenuItem
                      onClick={() => {
                        borrarSesionUsuario();
                        handleClose();
                      }}
                    >
                      <ListItemIcon>
                        <Logout fontSize="small" />
                      </ListItemIcon>
                      Cerrar sesión
                    </MenuItem>
                  </Menu>
                </Stack>
              </>
            )}
          </Toolbar>
        </AppBar>
        {opciones && (
          <Drawer
            variant="permanent"
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              [`& .MuiDrawer-paper`]: {
                width: drawerWidth,
                boxSizing: "border-box",
              },
            }}
          >
            <Toolbar />
            <Box sx={{ overflow: "auto" }}>{opciones}</Box>
          </Drawer>
        )}
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
          <Toolbar />
          {children}
        </Box>
      </Box>
    </>
  );
};

export default NavBarDrawer;
