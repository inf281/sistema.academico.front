import Head from "next/head";
import { useForm } from "react-hook-form";
import { LoginType } from "@/modules/login/types/loginTypes";
import { FormInputText } from "@/components/form";
import { useEffect } from "react";
import { useAuth } from "@/components/context/auth";
import { useRouter } from "next/router";
import { Typography } from "@mui/material";
import NavBarDrawer from "@/components/ui/NarBarDrawer";
import { RolesUsuario } from "../../config";
import { imprimir } from "@/components/utils/imprimir";

export default function LoginPage() {
  const { ingresar, progresoLogin, estaAutenticado, rolUsuario, usuario } =
    useAuth();
  const { handleSubmit, control } = useForm<LoginType>({
    defaultValues: {
      correo: "lenny@flores.ku",
      contrasena: "12345678",
    },
  });
  const router = useRouter();
  const iniciarSesion = async ({ correo, contrasena }: LoginType) => {
    await ingresar({ correo, contrasena });
  };

  useEffect(() => {
    if (estaAutenticado && rolUsuario) cambiaRutaRol(rolUsuario);
  }, [usuario]);
  const cambiaRutaRol = async (rol: string) => {
    switch (rol) {
      case RolesUsuario.ADMINISTRADOR:
        await router.replace({
          pathname: "/administrador",
        });
        break;
      case RolesUsuario.PARTICIPANTE:
        await router.replace({
          pathname: "/participante",
        });
        break;
      case RolesUsuario.CONTROL:
        await router.replace({
          pathname: "/control",
        });
        break;

      case RolesUsuario.EXPOSITOR:
        await router.replace({
          pathname: "/expositor",
        });
        break;
      default:
        imprimir("entra aqui");
        await router.replace({
          pathname: "/ ",
        });

        break;
    }
  };
  return (
    <>
      <Head>
        <title>Iniciar Sesión</title>
      </Head>
      <NavBarDrawer
        children={
          <>
            <div className="container mt-5">
              <div className="row d-flex justify-content-center">
                <div className="col-md-6">
                  <div className="card px-5 py-5" id="form1">
                    <Typography
                      variant="inherit"
                      component="h2"
                      sx={{ color: "black", pb: 5, textAlign: "center" }}
                    >
                      INGRESE SUS CREDENCIALES
                    </Typography>
                    <div className="form-data" v-if="!submitted">
                      <form onSubmit={handleSubmit(iniciarSesion)}>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"correo"}
                            control={control}
                            name="correo"
                            label="Correo Electrónico"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"email"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"contrasena"}
                            control={control}
                            name="contrasena"
                            label="Contraseña"
                            size={"medium"}
                            type={"password"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          {" "}
                          <button className="btn btn-dark w-100" type="submit">
                            Iniciar Sesion
                          </button>
                        </div>
                      </form>
                      <div className="mb-3">
                        {" "}
                        <button
                          className="btn btn-dark w-100"
                          onClick={async () =>
                            await router.replace({
                              pathname: "/register",
                            })
                          }
                        >
                          Registrate
                        </button>
                      </div>{" "}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        }
      />
    </>
  );
}
