import dayjs from "dayjs";
import Head from "next/head";
import React, { ReactNode, useEffect, useState } from "react";
import { useAuth } from "@/components/context/auth";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { EventoType } from "@/components/types/usuariosCRUDTypes";
import { Box, Button, Grid, Paper, Tab, Tabs, Typography } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { Constantes, EstadosReserva } from "../../config";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import NavBarDrawer from "@/components/ui/NarBarDrawer";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function LoginPage() {
  const [value, setValue] = React.useState(0);

  const { usuario } = useAuth();
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [eventosData, setEventosData] = useState<EventoType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombre del Evento" },
    { campo: "persona", nombre: "Hora de inicio" },
    { campo: "persona", nombre: "Hora de fin" },
    { campo: "correo", nombre: "Fecha" },
    { campo: "rol", nombre: "Tipo" },
    { campo: "edad", nombre: "Modalidad" },
    { campo: "edad", nombre: "Ambiente" },
    { campo: "acciones", nombre: "Acciones" },
  ];
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    EventoType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = eventosData.map(
    (eventoData, indexEvento) => [
      <Typography
        key={`${eventoData.id}-${indexEvento}-nombres`}
        variant={"body2"}
      >
        {`${eventoData.nombreEvento}`}
      </Typography>,
      <div key={`${eventoData.id}-${indexEvento}-apellidos`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horaInicio).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${dayjs(eventoData.horafin).format(
          "HH:mm"
        )}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-edad`}>
        <Typography variant={"body2"}>{`${eventoData.fecha}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.tipo}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${eventoData.modalidad}`}</Typography>
      </div>,
      <div key={`${eventoData.id}-${indexEvento}-correo`}>
        <Typography variant={"body2"}>{`${
          eventoData.ambiente?.nombreAmbiente ?? "Sin asignar"
        }`}</Typography>
      </div>,
      /*       <Grid key={`${eventoData.id}-${indexEvento}-rol`}>
        <Chip
          key={`usuario-rol-${eventoData.estado}`}
          label={eventoData.estado}
        />
      </Grid>, */

      <Grid key={`${eventoData.id}-${indexEvento}-acciones`}>
        {value === 0 ? (
          <Button
            key={`accionAgregarUsuarioBoton`}
            variant={"contained"}
            sx={{ ml: 1, mr: 1, textTransform: "none" }}
            color="secondary"
            size={"small"}
            onClick={() => {
              imprimir(`reservaremos`, eventoData);
              reservar(eventoData);
            }}
          >
            Reservar
          </Button>
        ) : value === 1 ? (
          <Button
            key={`accionAgregarUsuarioBoton`}
            variant={"contained"}
            sx={{ ml: 1, mr: 1, textTransform: "none" }}
            color="error"
            size={"small"}
            onClick={() => {
              imprimir(`reservaremos`, eventoData);
              cancelar(eventoData);
            }}
          >
            Cancelar
          </Button>
        ) : value === 2 ? (
          eventoData?.reserva[0]?.estado === EstadosReserva.CERTIFICADO ? (
            <Button
              key={`accionAgregarUsuarioBoton`}
              variant={"contained"}
              sx={{ ml: 1, mr: 1, textTransform: "none" }}
              color="secondary"
              size={"small"}
              onClick={() => {
                imprimir(`reservaremos`, eventoData);
                cancelar(eventoData);
              }}
            >
              Descargar Certificado
            </Button>
          ) : (
            <Typography>No disponible</Typography>
          )
        ) : null}
      </Grid>,
    ]
  );
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerEventos();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
  ];
  useEffect(() => {
    obtenerEventos();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite, value]);
  const obtenerEventos = async () => {
    try {
      setLoading(true);
      let dir = "";
      switch (value) {
        case 0:
          dir = "eventosDisponibles";
          break;
        case 1:
          dir = "eventosReservador";
          break;
        case 2:
          dir = "eventosAsistidos";
          break;
        default:
          break;
      }
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/${dir}/${usuario?.id}`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setEventosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  const reservar = async (evento: EventoType) => {
    try {
      //setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/reserva`,
        tipo: "post",
        body: {
          idUser: usuario?.id,
          idEvento: evento.id,
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      //accionCorrecta();
    } catch (e) {
      imprimir(`Error al reservar el evento: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      obtenerEventos();
    }
  };
  const cancelar = async (evento: EventoType) => {
    try {
      //setLoadingModal(true);
      //await delay(1000);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/eventos/cancelar`,
        tipo: "post",
        body: {
          idUser: usuario?.id,
          idEvento: evento.id,
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
      //accionCorrecta();
    } catch (e) {
      imprimir(`Error al reservar el evento: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      obtenerEventos();
    }
  };
  return (
    <>
      <Head>
        <title>Participante</title>
      </Head>

      <NavBarDrawer
        children={
          <>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Paper sx={{ m: 1 }}>
                <Box sx={{ width: "80vh" }}>
                  <Typography
                    variant="inherit"
                    component="h2"
                    sx={{ color: "black", pt: 1, pb: 1, textAlign: "center" }}
                  >
                    Bienvenid@ {usuario?.nombre + " " + usuario?.apellido}
                  </Typography>
                  <Typography
                    variant="inherit"
                    component="h2"
                    sx={{ color: "black", pb: 2, textAlign: "center" }}
                  >
                    Usted tiene un rol de {usuario?.rol}
                  </Typography>
                </Box>
              </Paper>
            </Box>
            <CustomDialog
              isOpen={modalUsuario}
              handleClose={cerrarModalUsuario}
              title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
              disableBackdropClick
            >
              {/* <VistaModalUsuario
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        /> */}
            </CustomDialog>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Box sx={{ width: "90%" }}>
                <Paper sx={{ p: 1 }}>
                  <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                    <Tabs
                      value={value}
                      onChange={handleChange}
                      aria-label="basic tabs example"
                    >
                      <Tab label="Eventos Disponibles" {...a11yProps(0)} />
                      <Tab label="Eventos Reservados" {...a11yProps(1)} />
                      <Tab label="Eventos Asistidos" {...a11yProps(2)} />
                    </Tabs>
                  </Box>
                  <TabPanel value={value} index={0}>
                    <Box sx={{ width: "100%" }}>
                      <CustomDataTable
                        titulo={"Eventos Disponibles"}
                        error={!!errorData}
                        cargando={loading}
                        acciones={acciones}
                        columnas={columnas}
                        contenidoTabla={contenidoTabla}
                        paginacion={
                          <Paginacion
                            pagina={pagina}
                            limite={limite}
                            total={total}
                            cambioPagina={setPagina}
                            cambioLimite={setLimite}
                          />
                        }
                      />
                    </Box>
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <Box sx={{ width: "100%" }}>
                      <CustomDataTable
                        titulo={"Eventos Reservado"}
                        error={!!errorData}
                        cargando={loading}
                        acciones={acciones}
                        columnas={columnas}
                        contenidoTabla={contenidoTabla}
                        paginacion={
                          <Paginacion
                            pagina={pagina}
                            limite={limite}
                            total={total}
                            cambioPagina={setPagina}
                            cambioLimite={setLimite}
                          />
                        }
                      />
                    </Box>
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <Box sx={{ width: "100%" }}>
                      <CustomDataTable
                        titulo={"Eventos Aceptados y Asistidos"}
                        error={!!errorData}
                        cargando={loading}
                        acciones={acciones}
                        columnas={columnas}
                        contenidoTabla={contenidoTabla}
                        paginacion={
                          <Paginacion
                            pagina={pagina}
                            limite={limite}
                            total={total}
                            cambioPagina={setPagina}
                            cambioLimite={setLimite}
                          />
                        }
                      />
                    </Box>
                  </TabPanel>
                </Paper>
              </Box>
            </Box>
          </>
        }
      />
    </>
  );
}
