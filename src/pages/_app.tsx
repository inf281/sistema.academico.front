import { AuthProvider } from "@/components/context/auth";
import { FullScreenLoadingProvider } from "@/components/context/ui";

import type { AppProps } from "next/app";
import { SnackbarProvider } from "notistack";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "../components/context/ThemeContext";
export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider>
      <FullScreenLoadingProvider>
        <SnackbarProvider maxSnack={1}>
          <CssBaseline />
          <AuthProvider>
            <Component {...pageProps} />
          </AuthProvider>
        </SnackbarProvider>
      </FullScreenLoadingProvider>
    </ThemeProvider>
  );
}
