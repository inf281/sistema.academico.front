import Head from "next/head";
import { ReactNode, useEffect, useState } from "react";
import { useAuth } from "@/components/context/auth";
import { CustomDataTable } from "@/components/ui/CustomDataTable";
import { ColumnaType } from "@/components/types";
import { UsuarioRolCRUDType } from "@/components/types/usuariosCRUDTypes";
import { Box, Button, Chip, Grid, Paper, Typography } from "@mui/material";
import { IconoTooltip } from "@/components/ui/IconoTooltip";
import { imprimir } from "@/components/utils/imprimir";
import { Paginacion } from "@/components/ui/Paginacion";
import { Constantes } from "../../config";
import { InterpreteMensajes } from "@/components/utils";
import { useAlerts, useSession } from "@/components/hooks";
import { CustomDialog } from "@/components/ui/CustomDialog";
import { VistaModalUsuario } from "@/modules/usuario/ModalUsuarios";
import NavBarDrawer from "@/components/ui/NarBarDrawer";

export default function LoginPage() {
  const { usuario } = useAuth();
  const { sesionPeticion } = useSession();
  const [loading, setLoading] = useState<boolean>(false);
  const [usuariosData, setUsuariosData] = useState<UsuarioRolCRUDType[]>([]);
  const [errorData, setErrorData] = useState<any>();
  const columnas: Array<ColumnaType> = [
    { campo: "nombre", nombre: "Nombres" },
    { campo: "persona", nombre: "Apellidos" },
    { campo: "correo", nombre: "Correo" },
    { campo: "rol", nombre: "Rol" },
    { campo: "edad", nombre: "Edad" },
    { campo: "acciones", nombre: "Telefono" },
    { campo: "acciones", nombre: "Dirección" },
    { campo: "acciones", nombre: "Acciones" },
  ];
  const [usuarioEdicion, setUsuarioEdicion] = useState<
    UsuarioRolCRUDType | undefined | null
  >();
  const [limite, setLimite] = useState<number>(10);
  const [pagina, setPagina] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [modalUsuario, setModalUsuario] = useState(false);
  const editarUsuarioModal = (usuario: UsuarioRolCRUDType) => {
    setUsuarioEdicion(usuario);
    setModalUsuario(true);
  };
  const { Alerta } = useAlerts();
  const contenidoTabla: Array<Array<ReactNode>> = usuariosData.map(
    (usuarioData, indexUsuario) => [
      <Typography
        key={`${usuarioData.id}-${indexUsuario}-nombres`}
        variant={"body2"}
      >
        {`${usuarioData.nombre}`}
      </Typography>,
      <div key={`${usuarioData.id}-${indexUsuario}-apellidos`}>
        <Typography variant={"body2"}>{`${usuarioData.apellido}`}</Typography>
      </div>,

      <div key={`${usuarioData.id}-${indexUsuario}-correo`}>
        <Typography variant={"body2"}>{`${usuarioData.correo}`}</Typography>
      </div>,
      <Grid key={`${usuarioData.id}-${indexUsuario}-rol`}>
        <Chip
          key={`usuario-rol-${usuarioData.rol.nombreRol}`}
          label={usuarioData.rol.nombreRol}
        />
      </Grid>,
      <div key={`${usuarioData.id}-${indexUsuario}-edad`}>
        <Typography variant={"body2"}>{`${usuarioData.edad}`}</Typography>
      </div>,
      <div key={`${usuarioData.id}-${indexUsuario}-edad`}>
        <Typography variant={"body2"}>{`${usuarioData.telefono}`}</Typography>
      </div>,
      <div key={`${usuarioData.id}-${indexUsuario}-edad`}>
        <Typography variant={"body2"}>{`${usuarioData.direccion}`}</Typography>
      </div>,
      <Grid key={`${usuarioData.id}-${indexUsuario}-acciones`}>
        <IconoTooltip
          titulo={"Editar"}
          color={"primary"}
          accion={() => {
            imprimir(`Editaremos`, usuarioData);
            editarUsuarioModal(usuarioData);
          }}
          desactivado={usuario?.id === usuarioData.id}
          icono={"edit"}
          name={"Editar usuario"}
        />
      </Grid>,
    ]
  );
  const acciones: Array<ReactNode> = [
    <IconoTooltip
      titulo={"Actualizar"}
      key={`accionActualizarUsuario`}
      accion={async () => {
        await obtenerUsuariosPeticion();
      }}
      icono={"refresh"}
      name={"Actualizar lista de usuario"}
    />,
    <Button
      key={`accionAgregarUsuarioBoton`}
      variant={"contained"}
      sx={{ ml: 1, mr: 1, textTransform: "none" }}
      color="secondary"
      size={"small"}
      onClick={() => {
        agregarUsuarioModal();
      }}
    >
      Agregar
    </Button>,
  ];
  useEffect(() => {
    obtenerUsuariosPeticion();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagina, limite]);
  const obtenerUsuariosPeticion = async () => {
    try {
      setLoading(true);
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios`,
        params: {
          pagina: pagina,
          limite: limite,
        },
      });
      if (respuesta.finalizado) {
        setUsuariosData(respuesta.datos?.filas);
        setTotal(respuesta.datos?.total);
        setErrorData(null);
      } else {
        Alerta({ mensaje: `Error al obtener la lista`, variant: "error" });
      }
    } catch (e) {
      imprimir(`Error al obtener usuarios`, e);
      setErrorData(e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  const agregarUsuarioModal = () => {
    setUsuarioEdicion(null);
    setModalUsuario(true);
  };
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };
  return (
    <>
      <Head>
        <title>Administrador</title>
      </Head>

      <NavBarDrawer
        children={
          <>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Paper sx={{ m: 1 }}>
                <Box sx={{ width: "80vh" }}>
                  <Typography
                    variant="inherit"
                    component="h2"
                    sx={{ color: "black", pt: 1, pb: 1, textAlign: "center" }}
                  >
                    Bienvenid@ {usuario?.nombre + " " + usuario?.apellido}
                  </Typography>
                  <Typography
                    variant="inherit"
                    component="h2"
                    sx={{ color: "black", pb: 2, textAlign: "center" }}
                  >
                    Usted tiene un rol de {usuario?.rol}
                  </Typography>
                </Box>
              </Paper>
            </Box>
            <CustomDialog
              isOpen={modalUsuario}
              handleClose={cerrarModalUsuario}
              title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
              disableBackdropClick
            >
              <VistaModalUsuario
                usuario={usuarioEdicion}
                accionCorrecta={() => {
                  cerrarModalUsuario().finally();
                  obtenerUsuariosPeticion().finally();
                }}
                accionCancelar={cerrarModalUsuario}
              />
            </CustomDialog>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Paper sx={{ p: 1 }}>
                <Box>
                  <CustomDataTable
                    titulo={"Usuarios"}
                    error={!!errorData}
                    cargando={loading}
                    acciones={acciones}
                    columnas={columnas}
                    contenidoTabla={contenidoTabla}
                    paginacion={
                      <Paginacion
                        pagina={pagina}
                        limite={limite}
                        total={total}
                        cambioPagina={setPagina}
                        cambioLimite={setLimite}
                      />
                    }
                  />
                </Box>
              </Paper>
            </Box>
          </>
        }
      />
    </>
  );
}
