import EventIcon from "@mui/icons-material/Event";
import List from "@mui/material/List";

import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import React, { useState } from "react";
import { useAuth } from "@/components/context/auth";
import { EventoType } from "@/components/types/usuariosCRUDTypes";
import { Box, Paper, Tab, Tabs, Typography } from "@mui/material";
import { CustomDialog } from "@/components/ui/CustomDialog";
import NavBarDrawer from "@/components/ui/NarBarDrawer";
import { useRouter } from "next/router";
import EventosExpositor from "@/modules/expositor/EventosExpositor";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function LoginPage() {
  const [value, setValue] = React.useState(0);
  const router = useRouter();
  const { usuario } = useAuth();

  const [usuarioEdicion, setUsuarioEdicion] = useState<
    EventoType | undefined | null
  >();
  const [modalUsuario, setModalUsuario] = useState(false);
  const cerrarModalUsuario = async () => {
    setModalUsuario(false);
    //await delay(500)
    setUsuarioEdicion(null);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <NavBarDrawer
      opciones={
        <>
          <List>
            <ListItem disablePadding>
              <ListItemButton
                selected={router.pathname === "/expositor"}
                onClick={async () => {
                  await router.replace({
                    pathname: "/expositor",
                  });
                }}
              >
                <ListItemIcon>
                  <EventIcon />
                </ListItemIcon>
                <ListItemText primary={"EVENTOS"} />
              </ListItemButton>
            </ListItem>
          </List>
        </>
      }
      children={
        <>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Paper sx={{ m: 1 }}>
              <Box sx={{ width: "80vh" }}>
                <Typography
                  variant="inherit"
                  component="h2"
                  sx={{ color: "black", pt: 1, pb: 1, textAlign: "center" }}
                >
                  Bienvenid@ {usuario?.nombre + " " + usuario?.apellido}
                </Typography>
                <Typography
                  variant="inherit"
                  component="h2"
                  sx={{ color: "black", pb: 2, textAlign: "center" }}
                >
                  Usted tiene un rol de {usuario?.rol}
                </Typography>
              </Box>
            </Paper>
          </Box>
          <CustomDialog
            isOpen={modalUsuario}
            handleClose={cerrarModalUsuario}
            title={usuarioEdicion ? "Editar usuario" : "Nuevo usuario"}
            disableBackdropClick
          >
            {/* <VistaModalUsuario
          usuario={usuarioEdicion}
          accionCorrecta={() => {
            cerrarModalUsuario().finally();
            obtenerEventos().finally();
          }}
          accionCancelar={cerrarModalUsuario}
        /> */}
          </CustomDialog>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Box sx={{ width: "100%" }}>
              <Paper sx={{ p: 1 }}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="basic tabs example"
                  >
                    <Tab label="Eventos" {...a11yProps(0)} />
                    {/* <Tab label="Eventos Asistidos" {...a11yProps(2)} /> */}
                  </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                  <Box sx={{ width: "100%" }}>
                    <EventosExpositor />
                  </Box>
                </TabPanel>
              </Paper>
            </Box>
          </Box>
        </>
      }
    />
  );
}
