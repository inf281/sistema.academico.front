import { useAuth } from "@/components/context/auth";
import { useAlerts, useSession } from "@/components/hooks";
import Head from "next/head";
import Link from "next/link";
import { useState } from "react";
interface datosPagina {
  nombreInstitucion: string;
  mision: string;
  vision: string;
  quienes: string;
  objetivo: string;
}

export default function Home() {
  const { estaAutenticado, datosPagina } = useAuth();
  return (
    <>
      <Head>
        <title>{datosPagina.nombreInstitucion}</title>
      </Head>
      <div id="page-top">
        <nav
          className="navbar navbar-expand-lg navbar-light fixed-top py-3"
          id="mainNav"
        >
          <div className="container px-4 px-lg-5">
            <a className="navbar-brand" href="#page-top">
              {datosPagina.nombreInstitucion}
            </a>
            <button
              className="navbar-toggler navbar-toggler-right"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarResponsive"
              aria-controls="navbarResponsive"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ms-auto my-2 my-lg-0">
                <li className="nav-item">
                  <a className="nav-link" href="#about">
                    Acerca de Nosotros
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#services">
                    Propósito
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#portfolio">
                    Eventos Recientes
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#contact">
                    Contactanos
                  </a>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" href="/LoginPage">
                    {estaAutenticado ? "Inicio" : "Iniciar Sesión"}
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        {/* Masthead*/}
        <header className="masthead">
          <div className="container px-4 px-lg-5 h-100">
            <div className="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
              <div className="col-lg-8 align-self-end">
                <h1 className="text-white font-weight-bold">
                  Tu lugar favorito para realizar tus eventos universitarios
                </h1>
                <hr className="divider" />
              </div>
              <div className="col-lg-8 align-self-baseline">
                <p className="text-white-75 mb-5">
                  Empieza registrando tu evento y te prestamos lo demas para
                  realizar tu evento
                </p>
                <a className="btn btn-primary btn-xl" href="#about">
                  Conoce mas
                </a>
              </div>
            </div>
          </div>
        </header>
        {/* About*/}
        <section className="page-section bg-primary" id="about">
          <div className="container px-4 px-lg-5">
            <div className="row gx-4 gx-lg-5 justify-content-center">
              <div className="col-lg-8 text-center">
                <h2 className="text-white mt-0">Acerca de Nosotros</h2>
                <hr className="divider divider-light" />
                <p className="text-white-75 mb-4">{datosPagina.quienes}</p>

                <Link className="btn btn-light btn-xl" href="/LoginPage">
                  {estaAutenticado ? "Inicio" : "Empieza"}
                </Link>
              </div>
            </div>
          </div>
        </section>
        {/* Services*/}
        <section className="page-section" id="services">
          <div className="container px-4 px-lg-5">
            <h2 className="text-center mt-0">Propósito de la institución</h2>
            <hr className="divider" />
            <div className="row gx-4 gx-lg-5">
              <div className="col-lg-4 col-md-6 text-center">
                <div className="mt-5">
                  <div className="mb-2">
                    <i className="bi-gem fs-1 text-primary"></i>
                  </div>
                  <h3 className="h4 mb-2">Misión</h3>
                  <p className="text-muted mb-0">{datosPagina.mision}</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 text-center">
                <div className="mt-5">
                  <div className="mb-2">
                    <i className="bi-laptop fs-1 text-primary"></i>
                  </div>
                  <h3 className="h4 mb-2">Visión</h3>
                  <p className="text-muted mb-0">{datosPagina.vision}</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 text-center">
                <div className="mt-5">
                  <div className="mb-2">
                    <i className="bi-globe fs-1 text-primary"></i>
                  </div>
                  <h3 className="h4 mb-2">Objetivos</h3>
                  <p className="text-muted mb-0">{datosPagina.objetivo}</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Portfolio*/}
        <div id="portfolio">
          <div className="container-fluid p-0">
            <div className="row g-0">
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/1.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/1.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/2.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/2.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/3.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/3.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/4.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/4.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/5.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/5.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
              <div className="col-lg-4 col-sm-6">
                <a
                  className="portfolio-box"
                  href="../images/portfolio/fullsize/6.jpg"
                  title="Project Name"
                >
                  <img
                    className="img-fluid"
                    src="../images/portfolio/thumbnails/6.jpg"
                    alt="..."
                  />
                  <div className="portfolio-box-caption p-3">
                    <div className="project-category text-white-50">
                      Category
                    </div>
                    <div className="project-name">Project Name</div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* Contact*/}
        <section className="page-section" id="contact">
          <div className="container px-4 px-lg-5">
            <div className="row gx-4 gx-lg-5 justify-content-center">
              <div className="col-lg-8 col-xl-6 text-center">
                <h2 className="mt-0">Envianos un Mensaje</h2>
                <hr className="divider" />
                <p className="text-muted mb-5">
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Nisi, voluptate sunt quidem dolores alias, soluta rem placeat
                  nemo voluptas voluptates fugit! Alias iste quidem molestias
                  similique debitis ipsa quia eos?
                </p>
              </div>
            </div>
            <div className="row gx-4 gx-lg-5 justify-content-center mb-5">
              <div className="col-lg-6">
                {/* * * * * * * * * * * * * * * **/}
                {/* * * SB Forms Contact Form * **/}
                {/* * * * * * * * * * * * * * * **/}
                {/* This form is pre-integrated with SB Forms.*/}
                {/* To make this form functional, sign up at*/}
                {/* https://startbootstrap.com/solution/contact-forms*/}
                {/* to get an API token!*/}
                <form id="contactForm" data-sb-form-api-token="API_TOKEN">
                  {/* Name input*/}
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="name"
                      type="text"
                      placeholder="Enter your name..."
                      data-sb-validations="required"
                    />
                    <label htmlFor="name">Nombre Completo</label>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="name:required"
                    >
                      El nombre es requerido.
                    </div>
                  </div>
                  {/* Email address input*/}
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="email"
                      type="email"
                      placeholder="name@example.com"
                      data-sb-validations="required,email"
                    />
                    <label htmlFor="email">Correo Electronico</label>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="email:required"
                    >
                      Correo requerido.
                    </div>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="email:email"
                    >
                      Correo no valido.
                    </div>
                  </div>
                  {/* Phone number input*/}
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="phone"
                      type="tel"
                      placeholder="(123) 456-7890"
                      data-sb-validations="required"
                    />
                    <label htmlFor="phone">Numero de Celular</label>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="phone:required"
                    >
                      Celular requerido.
                    </div>
                  </div>
                  {/* Message input*/}
                  <div className="form-floating mb-3">
                    <textarea
                      className="form-control"
                      id="message" /* type="text" */
                      placeholder="Enter your message here..." /* style="height: 10rem" */
                      data-sb-validations="required"
                    ></textarea>
                    <label htmlFor="message">Mensaje</label>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="message:required"
                    >
                      Mensaje requerido.
                    </div>
                  </div>
                  {/* Submit success message*/}
                  {/**/}
                  {/* This is what your users will see when the form*/}
                  {/* has successfully submitted*/}
                  <div className="d-none" id="submitSuccessMessage">
                    <div className="text-center mb-3">
                      <div className="fw-bolder">
                        Form submission successful!
                      </div>
                      To activate this form, sign up at
                      <br />
                      <a href="https://startbootstrap.com/solution/contact-forms">
                        https://startbootstrap.com/solution/contact-forms
                      </a>
                    </div>
                  </div>
                  {/* Submit error message*/}
                  {/**/}
                  {/* This is what your users will see when there is*/}
                  {/* an error submitting the form*/}
                  <div className="d-none" id="submitErrorMessage">
                    <div className="text-center text-danger mb-3">
                      Error sending message!
                    </div>
                  </div>
                  {/* Submit Button*/}
                  <div className="d-grid">
                    <button
                      className="btn btn-primary btn-xl disabled"
                      id="submitButton"
                      type="submit"
                    >
                      Enviar Mensaje
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="row gx-4 gx-lg-5 justify-content-center">
              <div className="col-lg-4 text-center mb-5 mb-lg-0">
                <i className="bi-phone fs-2 mb-3 text-muted"></i>
                <div>+591 78946546</div>
              </div>
            </div>
          </div>
        </section>

        <footer className="bg-light py-5">
          <div className="container px-4 px-lg-5">
            <div className="small text-center text-muted">
              Los Derechos Reservados &copy; 2023 - SGes-EventosA
            </div>
          </div>
        </footer>

        {/*         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.js"></script>

        <script src="../static/js/scripts.js"></script>

        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> */}
      </div>
    </>
  );
}
