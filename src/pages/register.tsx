import Head from "next/head";
import { useForm } from "react-hook-form";
import { UsuarioRegisterType } from "@/modules/login/types/loginTypes";
import { FormInputDropdown, FormInputText } from "@/components/form";
import { useState } from "react";
import { useAuth } from "@/components/context/auth";
import { useAlerts, useSession } from "@/components/hooks";
import { Constantes } from "../../config";
import { InterpreteMensajes } from "@/components/utils";
import { imprimir } from "@/components/utils/imprimir";
import NavBarDrawer from "@/components/ui/NarBarDrawer";

export default function LoginPage() {
  const { progresoLogin } = useAuth();
  const [loading, setLoading] = useState<boolean>(false);
  const { Alerta } = useAlerts();
  const { sesionPeticion } = useSession();
  const { handleSubmit, control, watch } = useForm<UsuarioRegisterType>({
    defaultValues: {
      correo: "",
      nombre: "",
      sexo: "",
      apellido: "",
      contrasena: "",
      edad: undefined,
      telefono: undefined,
      direccion: "",
      repiteContrasena: "",
    },
  });
  const guardarUsuariosPeticion = async (usuario: UsuarioRegisterType) => {
    setLoading(true);
    try {
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/usuarios/create`,
        tipo: "post",
        body: {
          ...usuario,
          edad: Number(usuario.edad),
          telefono: Number(usuario.telefono),
        },
      });
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: "success",
      });
    } catch (e) {
      imprimir(`Error al crear o actualizar usuario: `, e);
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: "error" });
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Head>
        <title>Registrate</title>
      </Head>
      <NavBarDrawer
        children={
          <>
            <form onSubmit={handleSubmit(guardarUsuariosPeticion)}>
              <div className="container">
                <div className="row d-flex justify-content-center">
                  <div className="col-md-6">
                    <div className="card px-5 py-5" id="form1">
                      <div className="form-data" v-if="!submitted">
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"correo"}
                            control={control}
                            name="correo"
                            label="Correo Electrónico"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"email"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"contrasena"}
                            control={control}
                            name="contrasena"
                            label="Contraseña"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"password"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"repiteContrasena"}
                            control={control}
                            name="repiteContrasena"
                            label="Repita la contraseña"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"password"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                              validate: (value) => {
                                if (value !== watch("contrasena"))
                                  return "Las contraseñas son distintas";
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"nombre"}
                            control={control}
                            name="nombre"
                            label="Nombres"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"text"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"apellido"}
                            control={control}
                            name="apellido"
                            label="Apellidos"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"text"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"edad"}
                            control={control}
                            name="edad"
                            label="Edad"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"number"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"direccion"}
                            control={control}
                            name="direccion"
                            label="Dirección"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"text"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                              minLength: {
                                value: 3,
                                message: "Mínimo 3 caracteres",
                              },
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputText
                            id={"telefono"}
                            control={control}
                            name="telefono"
                            label="Telefóno"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            type={"number"}
                            disabled={progresoLogin}
                            rules={{
                              required: "Este campo es requerido",
                            }}
                          />
                        </div>
                        <div className="forms-inputs mb-4">
                          <FormInputDropdown
                            id={"sexo"}
                            control={control}
                            name="sexo"
                            label="Sexo"
                            size={"medium"}
                            labelVariant={"subtitle1"}
                            disabled={progresoLogin}
                            options={[
                              {
                                key: "M",
                                value: "M",
                                label: "Masculino",
                              },
                              {
                                key: "F",
                                value: "F",
                                label: "Femenino",
                              },
                            ]}
                            rules={{
                              required: "Este campo es requerido",
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          {" "}
                          <button
                            className="btn btn-dark w-100"
                            type="submit"
                            disabled={loading}
                          >
                            Registrate
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </>
        }
      />
    </>
  );
}
